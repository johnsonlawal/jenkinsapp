#
#Created by Johnson Lawal on 11/10/2017.
#

import jenkins
import requests
import datetime
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

def get_server_instance():
	#connect to the db and return the server instance
    jenkins_url = 'http://localhost:8080'
    server = jenkins.Jenkins(jenkins_url, username='admin123', password='547c5058838349deb6a4f7791531025a')
    return server

def saveJob(session, jlist):
    for j in jlist:
        session.add(j)
    session.commit()

def getLastJobId(session, name):
    job = session.query(Jobs).filter_by(name=name).order_by(Jobs.jen_id.desc()).first()
    if (job != None):
        return job.jen_id
    else:
        return None

def createDbSession():
    engine = create_engine('sqlite:///jenkinsjobsstate.db', echo=False)
    session = sessionmaker(bind=engine)()
    Base.metadata.create_all(engine)
    return session
		
class Jobs(Base):
    __tablename__ = 'Jobs'

    id = Column(Integer, primary_key = True)
    jen_id = Column(Integer)
    name = Column(String)
    timeStamp = Column(DateTime)
    result = Column(String)
    building = Column(String)
    estimatedDuration = Column(String)

def createJobList(start, lastBuildNumber, jobName):
    jList = []
    for i in range(start + 1, lastBuildNumber + 1):
        current = server.get_build_info(jobName, i)
        current_as_jobs = Jobs()
        current_as_jobs.jen_id = current['id']
        current_as_jobs.building = current['building']
        current_as_jobs.estimatedDuration = current['estimatedDuration']
        current_as_jobs.name = jobName
        current_as_jobs.result = current['result']
        current_as_jobs.timeStamp = datetime.datetime.fromtimestamp(int(current['timestamp'])*0.001)
        jList.append(current_as_jobs)
    return jList

#job details of retrieved job in the Jenkins instance
def get_job_details():
    server = get_server_instance()
    for job_instance in server.get_all_jobs():
        print ('Job Name:%s' % (job_instance.name))
        print ('Job Description:%s' % (job_instance.get_description()))
        print ('Is Job running:%s' % (job_instance.is_running()))
        print ('Is Job enabled:%s' % (job_instance.is_enabled()))

server = get_server_instance()

authenticated = false
try:
    server.get_whoami()
    authenticated = true
except jenkins.JenkinsException as e:
    print ('Error: Unable to authenticate')
    authenticated = false

if authenticated:
    session = createDbSession()
	
    jobs = server.get_all_jobs()
    for j in jobs:
		# get a job by name
        jobName = j['name']
        lastJobId = getLastJobId(session, jobName) # get by name
        lastBuildNumber = server.get_job_info(jobName)['lastBuild']['number']  # get last build number from Jenkins for this job
        
        # when job not stored, update database
        if lastJobId == None:
            start = 0
        # when job exists, update database
        else:
            start = lastJobId

        # list of job objects to be saved to the database
        jlist = createJobList(start, lastBuildNumber, jobName)
        saveJob(session, jlist)